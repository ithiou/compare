<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('libProduct')->index();
            $table->string('slug')->nullable();
            $table->longText('descProduct')->nullable();
            $table->integer('priceProduct')->unsigned()->default(0)->index();
            $table->text('imgProduct');
            $table->string('numSeller')->nullable();
            $table->string('src')->nullable();
            $table->string('urlProduct');
            $table->string('logo')->nullable();
            $table->integer('origin');
            $table->integer('subcategory_id')->unsigned()->index();
            $table->foreign('subcategory_id')->references('id')->on('subcategories')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
