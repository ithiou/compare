<?php

namespace App\Http\Controllers;

use App\Category;
use App\Product;
use App\Subcategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class SearchController extends Controller
{
    public function products(Request $request)
    {
        $keyword = $request->q;
        $words = [];
        $results = [];
        $array = explode(' ', $keyword);
        $products = \App\Product::with('subcategory');
        foreach($array as $key)
        {
            $products = $products->where('libProduct', 'LIKE', '%'.$key.'%');
        }
        $products = $products->get();
        //dd($products);
        foreach($products as $product)
        {
            $all = "";
            foreach($array as $key)
            {
                $items = explode(' ', $product->libProduct);
                foreach ($items as $item)
                {
                    if (stripos($item, $key) > -1)
                    {
                        $all .= str_replace([')', '(', '...'], '', $item) . ' ';
                        break;
                    }
                }
            }
            $all = substr($all,0,strlen($all)-1);
            $all = ucfirst(strtolower($all));
            if(!empty($all))
            {
                $trouve = false;
                $json_struct = [
                    'name' => $all,
                    'from' => $product->subcategory->category->slug . '/' . $product->subcategory->slug
                ];
                foreach ($results as $result)
                {
                    if($result['name'] == $json_struct['name'] && $result['from'] == $json_struct['from'])
                    {
                        $trouve = true;
                        break;
                    }
                }
                if(!$trouve)
                    array_push($results,$json_struct);
            }
        }
        if(!empty($results))
            return json_encode($results);
        return '';
    }


    public function redirectroute(Request $request)
    {
        $key = $request->q;
        if(!preg_match('/[a-zA-Z0-9]+/',$key))
            return redirect(route('index'));
        $key = str_replace(['\'', ' '], '-', $key);
        return redirect(route('index.notFound',$key));
    }

    public function notFound(Request $request)
    {
        $q = $request->q;
        return view('not_found', compact('q'));
    }

    public function redirectroute2(Request $request)
    {
        $category = $request->category;
        $category = Category::where('slug',$category)->find(1);
        $subcategory = "";
        $q = "";
        $min = 0;
        $max = 1000000;
        $order = "asc";
        $source = 2;
        if(!empty($request->min))
            $min = $request->min;
        if(!empty($request->max))
            $max = $request->max;
        if(!empty($request->min))
            $order = $request->order;
        if(!empty($request->source))
            $source = $request->source;
        if(!empty($request->category) && !isset($request->subcategory) && empty($request->subcategory))
        {
            $category = $category->slug;
            return redirect()->route('search.results', compact('category'));
        }
        if(!empty($request->subcategory) && !isset($request->q) && empty($request->q))
        {
            $category = $category->slug;
            $subcategory = $request->subcategory;
            return redirect()
                ->route('search.results',
                    compact('category','subcategory','min','max','order','source'));
        }
        if(!empty($request->q))
        {
            $q = $request->q;
            $q = str_replace(['\'', ' '], '-', $q);
            $subcategory = $request->subcategory;
            $category = $category->slug;
            return redirect()
                ->route('search.results',
                    compact('category','subcategory','q','min','max','order','source'));
        }
    }

    public function results(Request $request)
    {
        $category = Category::where('slug',$request->category)->find(1);
        if(!empty($category))
        {
            $subcategory = "";
            $q = "";
            $products = $category->products()->where('priceProduct','>',0);
            $min_range = 0;
            $max_range = 1000000;
            if(!empty($request->subcategory))
            {
                $subcategory = Subcategory::where('slug',$request->subcategory)->get()->first();
                if($subcategory == null)
                    abort(404);
                $min_range = $subcategory->minPrice;
                $max_range = $subcategory->maxPrice;
                $products = $products->where('subcategory_id',$subcategory->id);
            }
            if(!empty($request->q))
            {
                $q = $request->q;
                $q = str_replace('-', ' ', $q);
                $array = explode(' ', $q);
                foreach($array as $key)
                {
                    $products = $products->where('libProduct', 'LIKE', '%'.$key.'%');
                }
            }
            if(!empty($request->min))
                $products = $products->where('priceProduct', '>=', $request->min);
            if(!empty($request->max))
                $products = $products->where('priceProduct', '<=', $request->max);
            if(!empty($request->source) && $request->source < 2)
                $products = $products->where('origin', $request->source);
            if($request->order == 'desc')
                $products = $products->orderBy('priceProduct','desc');
            else
                $products = $products->orderBy('priceProduct');
            $products = $products->paginate(12);
            return view('results',
                compact('category','subcategory','products', 'q', 'min_range','max_range'));
        }
        else
        {
            abort(404);
        }
    }

}
