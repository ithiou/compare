<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{

    public $timestamps = false;

    public function subcategory()
    {
        return $this->belongsTo('App\Subcategory');
    }

    public function getDescProductAttribute()
    {
        return substr($this->attributes['descProduct'], 0, 100) . "...";
    }

    public function getlibProductAttribute()
    {
        return substr($this->attributes['libProduct'], 0, 26) . "...";
    }

    public function getPriceProductAttribute()
    {
        return number_format($this->attributes['priceProduct'], 0, ',', ' ');
    }

}
