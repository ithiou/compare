@extends('default')

@push('css')
	<link rel="stylesheet" href="{{asset('css/main2.css')}}">
@endpush

@section('container')
	<header id="head">
		<div class="container banner" style="background-image: url( {{asset('images/slide_maison.jpg')}} );">
			<div class="row margin-top-5">
				<h1 class="lead">Comparateur de prix</h1>
				<p class="tagline2">
					Nous regroupons tous les sites de vente en ligne
				</p>
			</div>
			<div class="row">
				<div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
					{{-- Form::open(['url' => route('search'), 'autocomplete' => 'off']) --}}
					{!! Form::open() !!}
						<div class="custom-search-input col-md-12">
							<div class="input-group col-md-12">
								{!! Form::text('q',null,['id'=>'search','class'=>'form-control input-lg', 'placeholder' => 'Trouver au prix le plus bas', 'required']) !!}
								<span class="input-group-btn">
									<button class="btn btn-lg">
										<img src="{{asset('images/magnifying-glass.png')}}">
									</button>
								</span>
							</div>
						</div>
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</header>
@stop

@push('scripts')
	<script src="{{asset('js/typeahead.jquery.min.js')}}"></script>
	<script src="{{asset('js/bloodhound.min.js')}}"></script>
	<script src="{{asset('js/autocomplete.js')}}"></script>
@endpush