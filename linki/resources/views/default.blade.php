<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <meta name="viewport"    content="width=device-width, initial-scale=1.0">
    <meta name="description" content="@yield('description')">
    <meta name="author" content="Rodrigo Somoza">
    <title>
        @section('title')
            Comparez.co - Votre Comparateur par référence
        @show
    </title>
    <link rel="shortcut icon" href="{{asset('images/favicon.png')}}">
    <link rel="stylesheet" media="screen" href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,700">
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/bootstrap-theme.css')}}" media="screen" >
    @stack('css')
    <script src="{{asset('js/html5shiv.js')}}"></script>
    <script src="{{asset('js/respond.min.js')}}"></script>
    <![endif]-->
</head>

<body class="home">
@include('partials._nav')
@yield('container')
@yield('footer')
<script src="{{asset('/js/jquery.min.js')}}"></script>
<script src="{{asset('js/menu.js')}}"></script>
@stack('scripts')
</body>
</html>