<div class="navbar navbar-inverse navbar-fixed-top ban" >
    <div class="container-fluid no-margin no-padding">
        <div class="navbar-header col-xs-12 col-sm-2 col-md-2 no-margin">
            {{-- Bouton pour appareil mobil --}}
            {{--<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"><span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>--}}
            <a class="navbar-brand" href="{{route('index')}}"><img src="{{asset('images/logo_comparez.png')}}" alt="Linki - Référencement" style="margin-top:-15px;max-width: 100px;max-height: 100px;opacity: 1;"></a>
        </div>
        {{--<div class="navbar-collapse collapse">--}}
        <div class="col-xs-12 col-sm-10 col-md-10">
            @yield('search-bar-header')
            <ul class="nav navbar-nav pull-right">
                {{--
                <li class=""><a href="{{route('index')}}">Accueil</a></li>
                <li class=""><a href="{{url('a-propos')}}">A Propos</a></li>
                <li class=""><a href="{{url('contact')}}">Contact</a></li>
                <li><a class="btn" href="signin.html">SIGN IN / SIGN UP</a></li>
                --}}
            </ul>
        </div>
    </div>
</div>