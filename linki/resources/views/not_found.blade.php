@extends('default')

@push('css')
    <link rel="stylesheet" href="{{asset('css/jquery-ui.css')}}">
    <link rel="stylesheet" href="{{asset('css/main3.css')}}">
@endpush

@section('search-bar-header')

    <div class="col-md-8 col-md-offset-2">
        {!! Form::open(['class' => 'form-inline', 'style' => 'margin-top: 5px;margin-left: 5px;']) !!}
            <div class="custom-search-input flex-box">
                <div class="input-group col-md-9 flex-box" data-enhance=”false”>
                    @if(isset($q))
                        {!! Form::text('q', $q, ['id' => 'search', 'class' => 'form-control input-lg', 'placeholder' => 'Rechercher', 'style' => 'line-height:1.15em; border-collapse: separate; top: 0, left: 0; padding:3px;'] ) !!}
                    @else
                        {!! Form::text('q', null, ['id' => 'search', 'class' => 'form-control input-lg', 'placeholder' => 'Rechercher', 'style' => 'line-height:1.15em; border-collapse: separate; top: 0, left: 0; padding:3px;'] ) !!}
                    @endif
                        <span class="input-group-btn">
                        <button class="btn btn-lg" style="margin-top: -1px;">
                            <img src="{{asset('images/magnifying-glass.png')}}">
                        </button>
                    </span>
                </div>
            </div>
        {!! Form::close() !!}
    </div>
@stop

@section('container')
    <header id="head" class="thirdly"></header>
    <header>
        <div class="container-fluid no-padding">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <img src="{{asset('images/404.png')}}" class="img-responsive img-resolution2">
                </div>
            </div>
        </div>
    </header>
@stop

@push('scripts')
    <script src="{{asset('js/typeahead.jquery.min.js')}}"></script>
    <script src="{{asset('js/bloodhound.min.js')}}"></script>
    <script src="{{asset('js/autocomplete.js')}}"></script>
@endpush
