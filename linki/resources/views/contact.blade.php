@extends('default')
@section('container')
	<header id="head" class="thirdly"></header>

	<div class="container">

		<ol class="breadcrumb">
			<li><a href="{{url('/')}}">Accueil</a></li>
			<li class="active">A Propos</li>
		</ol>

		<div class="row">
			<article class="col-sm-9 maincontent">

				<header class="page-header">
					<h1 class="page-title">Contactez Nous</h1>
				</header>
				
				<p>
					Des questions? des avis? des contributions? Envoyez nous un message, nous vous répondons dans les jours qui viennent.
				</p>
				<br>
					{!! Form::open(['route' => 'contact.mail']) !!}
						<div class="row">
							<div class="col-sm-4">
								{!! Form::text('nom', null,['class'=>'form-control', 'placeholder' => 'Nom']) !!}
							</div>
							<div class="col-sm-4">
								{!! Form::email('email', null,['class'=>'form-control', 'placeholder' => 'Email']) !!}
							</div>
							<div class="col-sm-4">
								{!! Form::text('telephone', null,['class'=>'form-control', 'placeholder' => 'Téléphone']) !!}
							</div>
						</div>
						<br>
						<div class="row">
							<div class="col-sm-12">
								{!! Form::textarea('content', null,['class'=>'form-control', 'placeholder' => 'Rédiger votre message', 'rows' => 9]) !!}
							</div>
						</div>
						<br>
						<div class="row">
							<div class="col-sm-6">
								{!! Form::checkbox('newsletter', 1, false) !!}
								{!! Form::label('newsletter', '  Inscrivez vous à notre newsletter') !!}
							</div>
							<div class="col-sm-6 text-right">
								<input class="btn btn-purple" type="submit" value="Envoyer">
							</div>
						</div>
					{!! Form::close() !!}

			</article>

			<aside class="col-sm-3 sidebar sidebar-right">

				<div class="widget">
					<h4>Address</h4>
					<address>
						Senegal
					</address>
					<h4>Téléphone:</h4>
					<address>
						(+221) 77 567 89 98
					</address>
				</div>

			</aside>

		</div>
	</div>
@stop