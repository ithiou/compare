@extends('default')

@if(isset($subcategory) && !empty($subcategory))
@section('title')
    {!! $subcategory->title !!}
@stop

@section('description')
    {!! $subcategory->description !!}
@stop

@section('footer')
    <div id="footerwrap">
        <div class="container">
            <div class="row">
                <div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4 col-lg-4 col-lg-offset-4 text-center">
                    {!! $subcategory->meta_donnees !!}
                </div>
            </div>
        </div>
    </div>
@stop


@endif

@push('css')
    <link rel="stylesheet" href="{{asset('css/jquery-ui.css')}}">
    <link rel="stylesheet" href="{{asset('css/main3.css')}}">
    <style type="text/css">
        .ui-slider-horizontal .ui-state-default
        {
            background: url({{asset('images/sort-down.png')}}) no-repeat scroll 50% 50%;
            margin-top: -10px;
            border: none;
            cursor: pointer;
        }

    </style>
@endpush

@section('search-bar-header')
    <a href="javascript:" id="return-to-top"><span class="glyphicon glyphicon-chevron-up"></span></a>
    <div class="col-sm-3 col-md-8 col-md-offset-2 mobile">
        {!! Form::open(['route' => 'results','class' => 'form-inline', 'style' => 'margin-top: 5px;margin-left: 5px;']) !!}
        {!! Form::hidden('category',$category->slug) !!}
        @if(isset($subcategory) && !empty($subcategory))
            {!! Form::hidden('subcategory',$subcategory->slug) !!}
        @endif
            <div class="custom-search-input flex-box">
                <div class="input-group col-xs-12 col-md-9 flex-box mobile-search mobile-margin-right-20" data-enhance=”false”>
                    @if(isset($q) && !empty($q))
                        {!! Form::text('q', $q, ['id' => 'search', 'class' => 'form-control input-lg', 'placeholder' => 'Rechercher', 'style' => 'line-height:1.15em; border-collapse: separate; top: 0, left: 0; padding:3px;'] ) !!}
                    @else
                        {!! Form::text('q', null , ['id' => 'search', 'class' => 'form-control input-lg', 'placeholder' => 'Rechercher', 'style' => 'line-height:1.15em; border-collapse: separate; top: 0, left: 0; padding:3px;'] ) !!}
                    @endif
                        <span class="input-group-btn">
                        <button class="btn btn-lg" type="submit" style="margin-top: -1px;">
                            <img src="{{asset('images/magnifying-glass.png')}}">
                        </button>
                    </span>
                </div>
                <div class="input-group col-xs-12 col-md-3 search-bar-header-margin-left-45">
                    <span class="input-group-btn custom-btn">
                        <a class="btn btn-lg custom-btn btn-purple filtre hide-search mobile-button" data-command="toggle-search" data-toggle="tooltip" data-placement="top" title="Filtre" type="button" style="margin-top: -1px; border-radius: 8px;">
                            Filtres<span class="bold padding-bottom">+</span>
                        </a>
                    </span>
                </div>
            </div>
        {{-- Dropdown Filtre --}}
        <div class="row" style="display: none;">
            <div class="col-xs-12">
                <div class="input-group col-md-9 c-search">
                    <div class="container-fluid">
                        <div class="well cart">
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <div class="form-group">
                                        <h4 class="bold" style="font-family: 'Open sans', Helvetica, sans-serif;">
                                            Filtres :
                                        </h4>
                                        <h5>
                                            Prix : <span class="min"></span> - <span class="max"></span>
                                        </h5>
                                    </div>
                                </div>
                            </div>

                            <hr style="margin-bottom: 1px; margin-top: 1px;"/>

                            <div class="row">
                                {!! Form::hidden('min',null,['class' => 'min']) !!}
                                {!! Form::hidden('max',null,['class' => 'max']) !!}
                                <div class="col-sm-12">
                                    <div id="slider"></div>
                                </div>
                            </div>

                            <hr style="margin-bottom: 5px; margin-top: 15px; border-top: solid 2px #3678a0"/>

                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <div class="form-group">
                                        <h5>
                                            Ordre d'affichage :
                                        </h5>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    {!! Form::radio('order', 'asc', ['checked']) !!} Les moins chers en avant<br/>
                                    {!! Form::radio('order', 'desc') !!} Les plus chers en avant
                                </div>
                            </div>

                            <hr style="margin-bottom: 5px; margin-top: 15px; border-top: solid 2px #3678a0"/>

                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <div class="form-group">
                                        <h5>
                                            Comparer les produits provenant des :
                                        </h5>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    {!! Form::radio('source', '0') !!} Sites d'annonces <br/>
                                    {!! Form::radio('source', '1') !!} Sites e-commerce <br/>
                                    {!! Form::radio('source', '2', ['checked']) !!} Tous les sites
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 right">
                                    <button class="btn btn-sm bg-primary hv-white" type="submit" style="margin-top: -1px;">
                                        Valider
                                    </button>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
@stop

@section('container')
    <header id="head" class="thirdly"></header>
    <header>
        <div class="container-fluid">
            <div class="col-sm-12 text-center no-padding">
                <h1 id="header-text">
                    @if(!empty($subcategory))
                        {{$subcategory->libSubCategory}}
                    @endif
                    {{$category->libCategory}}
                     en Côte d'Ivoire
                </h1>
            </div>
        </div>
    </header>
    <header id="head">
        <div class="container">
            <?php
                $col = 0;
            ?>
            <div class="infinite-scroll">
                @forelse($products as $product)
                    @if($col == 0)
                        <div class="row">
                    @endif

                    <div class="col-sm-3 square">
                        <a href="{{$product->urlProduct}}" class="link-off text-center" target="_blank">
                            <center>
                                <img class="img-responsive img-resolution" src="{{$product->imgProduct}}" alt="{{$product->libProduct}}">
                            </center>
                            <hr>
                            <p class="paragraphe">
                                {{$product->libProduct}}
                            </p>
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-xs-6 col-sm-6 col-md-6 left blue gras bottom">
                                        {{$product->priceProduct}} Fcfa
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-md-6 right">
                                        <img src="{{$product->logo}}" class="img-responsive logo-resolution"></img>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                            <?php
                            $col++;
                            if($col == 4):
                                $col = 0;
                            ?>
                        </div>
                            <?php endif;?>
                @empty
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12 text-center" style="padding-top: 50px;">
                                <h2>Aucun résultat</h2>
                            </div>
                        </div>
                    </div>
                @endforelse
                    {{$products->links()}}
            </div>
        </div>
    </header>
@stop

@push('scripts')
    <script src="{{asset('js/typeahead.jquery.min.js')}}"></script>
    <script src="{{asset('js/bloodhound.min.js')}}"></script>
    <script src="{{asset('js/autocomplete.js')}}"></script>
    <script src="{{asset('js/jquery.jscroll.js')}}"></script>
    <script src="{{asset('js/scroll-to-top.js')}}"></script>
    <script src="{{asset('js/jquery-ui.js')}}"></script>
    <script src="{{asset('js/h2.js')}}"></script>
    <script type="text/javascript">
        $('ul.pagination').hide();
        $(function() {
            $('.infinite-scroll').jscroll({
                autoTrigger: true,
                loadingHtml: '<img class="center-block" src="{{asset('images/loading.gif')}}" alt="Loading..." />',
                padding: 0,
                nextSelector: '.pagination li.active + li a',
                contentSelector: 'div.infinite-scroll',
                callback: function() {
                    $('ul.pagination').remove();
                }
            });
        });
        $(function () {
            $('[data-command="toggle-search"]').on('click', function(event) {
                event.preventDefault();
                $(this).toggleClass('hide-search');

                if ($(this).hasClass('hide-search')) {
                    $('.c-search').closest('.row').slideUp(100);
                }else{
                    $('.c-search').closest('.row').slideDown(100);
                }
            })
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            var sliderDiv = $('#slider');

            sliderDiv.slider({
                range: true,
                min: {{$min_range}},
                max: {{$max_range}},
                values: [{{$min_range}}, {{$max_range}}],
                step: 1000,
                slide: function (event, ui) {
                    $('.min').val(ui.values[0]);
                    $('span.min').text(ui.values[0]);
                    $('.max').val(ui.values[1]);
                    $('span.max').text(ui.values[1]);
                }
            });
            $('.min').val(sliderDiv.slider('values', 0));
            $('span.min').text(sliderDiv.slider('values', 0));
            $('.max').val(sliderDiv.slider('values', 1));
            $('span.max').text(sliderDiv.slider('values', 1));
        });

    </script>
@endpush
