<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', ['as' => 'index', 'uses' => 'PagesController@index']);
Route::post('/', ['as' => 'index.post', 'uses' => 'SearchController@redirectroute']);
Route::get('/search/{q}', ['as' => 'index.notFound', 'uses' => 'SearchController@notFound']);

/* -- Systeme de recherche --*/
Route::get('search/product/json', ['as' => 'search.product.json', 'uses' => 'SearchController@products']);

Route::post('result', ['as' => 'results', 'uses' => 'SearchController@redirectroute2']);

Route::get('/{category}/{subcategory?}/{q?}/{min?}/{max?}/{order?}/{source?}', ['as' => 'search.results', 'uses' => 'SearchController@results']);


/*
Route::post('/{category}/{subcategory}/{q}', ['as' => 'search.results', 'uses' => 'SearchController@results']);

Route::get('contact', ['as' => 'contact.index', 'uses' => 'PagesController@contact']);
Route::post('contact', ['as' => 'contact.mail', 'uses' => 'PagesController@mail']);

Route::get('a-propos', function () {
    return view('a-propos');
});
*/
