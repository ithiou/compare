$(document).ready(function() {
    var bloodhound = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.whitespace('q'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: {
            url: '/search/product/json?q=%QUERY%',
            wildcard: '%QUERY%'
        },
    });

    $('#search').typeahead({
        hint: false,
        highlight: true,
        minLength: 2
    }, {
        name: 'q',
        source: bloodhound,
        display: function(data) {
            return data.name  //Input value to be set when you select a suggestion.
        },
        templates: {
            empty: [
                '<div class="list-group search-results-dropdown" style="margin-top:10px; color: #3678a0;"><div class="list-group-item">Aucun résultat</div></div>'
            ],
            header: [
                '<div style="margin:10px; color: #3678a0;">'
            ],
            suggestion: function(data) {
                return '<div style="font-weight:normal; color: #3678a0;" class="list-group-item"><a href="/'+ data.from +'" class="focused">' + data.name + ' <span class="float-right"><span class="gray">dans</span> ' + data.from +'</span></a></div>'
            }
        }
    }).bind("typeahead:selected", function (obj, datum, name) {
        //$("form").submit();
        var input = document.getElementById('search').value;
        window.location.href = '/' + datum.from + '/' + input;
    });
});
