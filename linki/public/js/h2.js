(function () {
    p = document.getElementsByClassName('paragraphe');
    for(var i = 0; i < p.length; i+=1)
    {
        var h2 = document.createElement('h2');
        h2.classList.add(p[i].classList);
        h2.innerText = p[i].innerText;
        p[i].parentNode.replaceChild(h2, p[i]);
    }
})();