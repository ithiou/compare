$(document).ready(function() {
    var bloodhound = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.whitespace,
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: {
            url: '/search/product/json?q=%QUERY%',
            wildcard: '%QUERY%'
        },
    });

    $('#search').typeahead({
        hint: false,
        highlight: true,
        minLength: 1
    }, {
        name: 'products',
        source: bloodhound,
        display: function(data) {
            return data  //Input value to be set when you select a suggestion.
        },
        templates: {
            empty: [
                '<div class="list-group search-results-dropdown" style="margin-top:10px; color: #3678a0;"><div class="list-group-item">Aucun résultat</div></div>'
            ],
            header: [
                '<div class="list-group search-results-dropdown" style="margin-top:10px; color: #3678a0;">'
            ],
            suggestion: function(data) {
                return '<div style="font-weight:normal; color: #3678a0;" class="list-group-item focused">' + data + '</div></div>'
            }
        }
    });
});